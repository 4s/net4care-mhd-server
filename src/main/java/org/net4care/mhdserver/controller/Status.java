package org.net4care.mhdserver.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@Configuration
@PropertySource(value="classpath:xds.properties")
public class Status {

  @Value("${xds.registryUrl}")
  private String registryUrl;

  @Value("${xds.repositoryUrl}")
  private String repositoryUrl;

  @RequestMapping(method=RequestMethod.GET, value="/Status/Ping", produces="text/plain; charset=utf-8")
  public ResponseEntity<String> ping() {
    return new ResponseEntity<String>("OK", HttpStatus.OK);
  }

  @RequestMapping(method=RequestMethod.GET, value="/Status/RegistryUrl", produces="text/plain; charset=utf-8")
  public ResponseEntity<String> getRegistryUrl() {
    return new ResponseEntity<String>(registryUrl, HttpStatus.OK);
  }

  @RequestMapping(method=RequestMethod.GET, value="/Status/RepositoryUrl", produces="text/plain; charset=utf-8")
  public ResponseEntity<String> getRepositoryUrl() {
    return new ResponseEntity<String>(repositoryUrl, HttpStatus.OK);
  }

  @RequestMapping(method=RequestMethod.GET, value="/Status/XdsUrls", produces="text/plain; charset=utf-8")
  public ResponseEntity<String> getXdsUrls() {
    return new ResponseEntity<String>("Registry: " + registryUrl + ", Repository: " + repositoryUrl, HttpStatus.OK);
  }
}
