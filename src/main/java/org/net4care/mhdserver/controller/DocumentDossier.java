package org.net4care.mhdserver.controller;

import org.net4care.xdsconnector.RegistryConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DocumentDossier {
	
	// MHD Get Document Dossier =XDS Registry Stored Query – GetDocuments
			
			
	@Autowired
	private RegistryConnector xdsRegistryConnector;
	
	
	@RequestMapping(method=RequestMethod.GET, value="/DocumentDossier/{entryUUID}/", params= "PatientID")
	public ResponseEntity<String> getDocumentDossier(@RequestParam("PatientID") String patientID, @PathVariable("entryUUID") String entryUUID){
	
		// ITI-66
		System.out.println("get document dossier");
		//patientID = CX encoded ie.
	
		if(!verifyEntryUUID(entryUUID) || !verifyPatientID(patientID)){
			System.out.println("invalid or missing entryuuid or patientid: "+entryUUID+", "+patientID);
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		if(nonexistantEntryUUID(entryUUID) || nonExistantPatientID(patientID)){
			System.out.println("input not found: "+entryUUID+">"+nonexistantEntryUUID(entryUUID)+" >"+patientID+">"+nonExistantPatientID(patientID));
			return new ResponseEntity<String>("Document Entry UUID not found", HttpStatus.NOT_FOUND);
		}

		//lookup document dossier
		
		if(isDocumentDeprecated()){
			System.out.println("Document is deprecated");
			return new ResponseEntity<String>("Document Entry UUID deprecated", HttpStatus.GONE);
		}
		
		if(ifRequestOtherwiseIllegal()){
			System.out.println("Otherwise illegal");
			return new ResponseEntity<String>("Request type not supported", HttpStatus.FORBIDDEN);
		}
		
		
		/*The Document Responder shall return the Document Dossier for the Document Entry
			represented by the patientID and entryUUID.
			The Document Responder shall verify that the
			documentDossier to be returned is
			consistent with the patientID.
			When the Document Responder is grouped with an XDS Document Consumer the Document
			Entry can be obtained through the use of the GetDocument query in the Registry Stored Query
			[ITI-18] transaction given the entryUUID
		 * 
		 */
		return null;
	}
	
	private boolean ifRequestOtherwiseIllegal() {
		// TODO Auto-generated method stub
		return false;
	}

	private boolean isDocumentDeprecated() {
		// TODO Auto-generated method stub
		return false;
	}

	private boolean nonExistantPatientID(String patientID) {
		// TODO Auto-generated method stub
		return false;
	}

	private boolean nonexistantEntryUUID(String entryUUID) {
		// TODO Auto-generated method stub
		return false;
	}

	private boolean verifyPatientID(String patientID) {
		// TODO Auto-generated method stub
		
		return false;
	}

	private boolean verifyEntryUUID(String entryUUID) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
