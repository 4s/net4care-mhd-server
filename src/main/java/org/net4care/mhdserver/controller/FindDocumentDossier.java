package org.net4care.mhdserver.controller;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URLEncoder;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBElement;

import org.net4care.mhdserver.model.DocumentDossierSearchResult;
import org.net4care.mhdserver.model.Dossier;
import org.net4care.xdsconnector.RegistryConnector;
import org.net4care.xdsconnector.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FindDocumentDossier {
	private static final boolean RETURN_404_ON_NO_DOCUMENTS_FOUND = false;
	private static final Format format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");

	@Autowired
	private RegistryConnector xdsRegistryConnector;
	
	@Value("${xds.repositoryUrl}")
	private String repositoryUrl;	

	@Value("${xds.registryUrl}")
	private String registryUrl;
	
	@Value("${server.address}")
	private String serverAddress;
	
	@Value("${server.port}")
	private String serverPort;
 
	@SuppressWarnings("unused")
	@RequestMapping(method=RequestMethod.GET, value="/DocumentDossier/search", produces="application/json", params= "PatientID")
	public ResponseEntity<DocumentDossierSearchResult> findDocumentDossiers(@RequestParam("PatientID") String patientID, HttpServletRequest request) throws MalformedURLException, UnsupportedEncodingException{

		List<AdhocQueryResponseType> queryResponses = xdsRegistryConnector.queryRegistry(patientID, request.getParameterMap());
		if (queryResponses == null) {
			return new ResponseEntity<DocumentDossierSearchResult>(HttpStatus.BAD_REQUEST);
		}
		else if (queryResponses.size() == 0) {
			return new ResponseEntity<DocumentDossierSearchResult>(HttpStatus.NOT_FOUND);
		}
		else if (queryResponses.get(0).getStatus().endsWith("Failure")) {
			// RegistryError error = queryResponses.get(0).getRegistryErrorList().getRegistryError().get(0);
			// String errorMessage = String.format("%s: %s %s", error.getErrorCode(), error.getCodeContext(), error.getValue());
			// TODO: add error message handling
			return new ResponseEntity<DocumentDossierSearchResult>(HttpStatus.BAD_REQUEST);
		}

		// Build the output.
		List<Dossier> dossiers = buildOutputDossiers(queryResponses, patientID);
		if (RETURN_404_ON_NO_DOCUMENTS_FOUND && dossiers.size() == 0) {
			return new ResponseEntity<DocumentDossierSearchResult>(HttpStatus.NOT_FOUND);
		}

		DocumentDossierSearchResult result = new DocumentDossierSearchResult(getOwnUrl(patientID), dossiers);
		return new ResponseEntity<DocumentDossierSearchResult>(result, HttpStatus.OK);
	}

	private List<Dossier> buildOutputDossiers(List<AdhocQueryResponseType> queryResponses, String patientID) throws UnsupportedEncodingException{
		List<Dossier> dossiers = new LinkedList<Dossier>();
		
		for(AdhocQueryResponseType response : queryResponses) {

			for(JAXBElement<? extends IdentifiableType> elm: response.getRegistryObjectList().getIdentifiable()){
				IdentifiableType type = elm.getValue();

				if (dossiers.indexOf(type.getId()) >= 0) continue;

				if (type instanceof ExtrinsicObjectType) {
					Dossier dossier = new Dossier();

					ExtrinsicObjectType extrinsicObject = (ExtrinsicObjectType) type;
					for (SlotType1 slot : extrinsicObject.getSlot()) {
						if ("creationTime".equals(slot.getName())) {
							dossier.setUpdated(slot.getValueList().getValue().get(0));
							break;
						}
					}
					
					for (ExternalIdentifierType eit: extrinsicObject.getExternalIdentifier()) {
						if (eit.getName().getLocalizedString().get(0).getValue().equals("XDSDocumentEntry.uniqueId")) {
							dossier.setId(eit.getValue());
							break;
						}
					}
					
					dossier.setDocumentName(extrinsicObject.getName().getLocalizedString().get(0).getValue());
					dossier.setRelated(getRelated(dossier.getId(), patientID));
					dossier.setSelf(getSelf(dossier.getId(), patientID));
					dossiers.add(dossier);
				}
			}			
		}
		
		return dossiers;
	}

	private String getUrl(String method, String id, String patientId) throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();
		sb.append("http://").append(serverAddress).append(":").append("port").append("/")
			.append(method).append("/").append(URLEncoder.encode(id, "utf-8")).append("/")
			.append("?PatientId=").append(URLEncoder.encode(patientId, "utf-8"));
		return sb.toString();
	}

	private URI getRelated(String id, String patientId) throws UnsupportedEncodingException {
		return URI.create(getUrl("Document", removePrefix(id), patientId));
	}

	private URI getSelf(String id, String patientId) throws UnsupportedEncodingException {
		return URI.create(getUrl("DocumentDossier", removePrefix(id), patientId));
	}
	
	private URI getOwnUrl(String patientId) throws UnsupportedEncodingException {
		return URI.create(getUrl("DocumentDossier", "search", patientId));
	}
	
	private String removePrefix(String id) {
		return (id.contains("urn:uuid:")) ? id.substring(9) : id;
	}
}
