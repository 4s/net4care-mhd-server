package org.net4care.mhdserver.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServletRequest;
import org.net4care.xdsconnector.RepositoryConnector;
import org.net4care.xdsconnector.service.RegistryError;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType.DocumentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Document {	
	@Autowired
	private RepositoryConnector xdsRepositoryConnector;
//http://localhost:8091/Document/urn:uuid:e35b0902-fcc5-4de6-a95b-15c147a8fda8/?PatientID=111
	//above does not work :/
	// MHD Get Document = 	XDS Retrieve Document Set
	// SOURCE:      http://www.ihe.net/uploadedFiles/Documents/ITI/IHE_ITI_Suppl_MHD.pdf
	// CHAPTER:     3.68
	// URI:         http://<location>/net.ihe/Document/<entryUUID>/?PatientID=<PatientID>
	// PatientID:   the CX encoded patient. See Section 3.65.4.1.2.2 Patient Identity. This value may need to be transformed for URL encoding.
	@RequestMapping(method=RequestMethod.GET, value="/Document/{entryUUID}/", params= "PatientID", produces="text/xml; charset=utf-8")
	public ResponseEntity<String> getDocument(@RequestParam("PatientID") String patientID, @PathVariable String entryUUID, HttpServletRequest request) throws UnsupportedEncodingException {

		if (nonexistantEntryUUID(entryUUID) || nonExistantPatientID(patientID)) {
			System.out.println("invalid input: >" + entryUUID + "<>" + patientID + "<");
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
		
		/* PatientID is currently not used
		if (!verifyPatientID(patientID)){
			System.out.println("input not found: " + entryUUID + ">" + nonexistantEntryUUID(entryUUID) + " >" + patientID + ">" + nonExistantPatientID(patientID) + "<");
			return new ResponseEntity<String>("Document Entry UUID not found", HttpStatus.NOT_FOUND);
		}*/
		System.out.println("looking up PatientId: "+patientID+" and "+entryUUID);
		String phmr = null;
		RetrieveDocumentSetResponseType response = xdsRepositoryConnector.retrieveDocumentSet(URLDecoder.decode(entryUUID, "utf-8"));
		if(!"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success".equals(response.getRegistryResponse().getStatus())){
			for(RegistryError registryError : response.getRegistryResponse().getRegistryErrorList().getRegistryError()){
				if("XDSUnknownRepositoryUniqueID".equals(registryError.getCodeContext())){
					return new ResponseEntity<String>("Document Entry UUID not found", HttpStatus.NOT_FOUND);		
				}
				//TODO add other registryerror types
			}
			return new ResponseEntity<String>("Request failed.", HttpStatus.NOT_FOUND);
		}
		
		if (response.getDocumentResponse().size() > 0) {
			//TODO Should we make a check to verify patientId of returned document matches queried patientId?
			DocumentResponse documentResponse = response.getDocumentResponse().get(0);
			// Do not give the UTF-8 encoding here.
			// The danish charecters comes back as code points not UTF-8 chars, e.g. '�' is 0xE6 instead of 0xC3 0xA6.
			phmr = new String(documentResponse.getDocument());
		}
		
		if (isDocumentDeprecated()){ 
			System.out.println("Document is deprecated"); //IF this is ok with sec policy, else return 404
			return new ResponseEntity<String>("Document Entry UUID deprecated", HttpStatus.GONE);
		}

		return new ResponseEntity<String>(phmr, HttpStatus.OK);
	}

	// Not in use
	private boolean isDocumentDeprecated() {
		return false;
	}

	// Patient ID is not used
	private boolean nonExistantPatientID(String patientID) {
		return false;
	}

	private boolean nonexistantEntryUUID(String entryUUID) {
		if (entryUUID == null || "".equals(entryUUID)) {
			return true;
		}
		
		return false;
	}
}