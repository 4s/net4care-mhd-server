package org.net4care.mhdserver.model;

import java.net.URI;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DocumentDossierSearchResult {
	private static final Format format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");

	private String updated;
	private URI self;
	private Dossier[] entries;

	public DocumentDossierSearchResult(URI self, List<Dossier> entries) {
		this.self = self;
		this.updated = format.format(new Date());
		this.entries = entries.toArray(new Dossier[]{});
	}

	public DocumentDossierSearchResult(URI self, String errorMessage) {
		this.self = self;
		this.updated = format.format(new Date());
		this.entries = new Dossier[]{};
	}

	public String getUpdated() { return updated; }
	public void setUpdated(String updated) { this.updated = updated; }
	
	public URI getSelf() {
		return self;
	}
	public void setSelf(URI self) {
		this.self = self;
	}
	
	public Dossier[] getEntries() {
		return entries;
	}
	public void setEntries(Dossier[] entries) {
		this.entries = entries;
	}
}
