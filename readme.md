# Net4Care MHD Server

The MHD Server is a standalone component implementing the IHE *Mobile access to Health Documents* (MHD) profile.

For more information see the [wiki page]([http://wiki.4s-online.dk/doku.php?id=net4care:mhd-server:overview).

## Governance
The project is governed by 4S, with source control on [Bitbucket](https://bitbucket.org/4s/net4care-mhd-server)
and issue tracking in [JIRA](https://issuetracker4s.atlassian.net/browse/MHD).

## Develop

The project depends on the [Net4Care XDS connector](https://bitbucket.org/4s/net4care-xds-connector) component,
which it will fetch from the [4S Maven repository](http://artifactory.4s-online.dk), if it is not built and
installed locally.

 - Clone project from Bitbucket:  
   `git clone https://bitbucket.org/4s/net4care-mhd-server.git`

 - Compile and run tests:  
   `mvn install`

 - Run the server:  
   `java -jar target/mhd-server.jar`
